<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }

    //Deze functie is verantwoordelijk voor het opslaan van een user in de database
    public function store()
    {
      //Onderstaande functie valideert de/het form
      $this->validate(request(),[
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required'
      ]);

      $user = new User;

      //Hier koppel ik het ingevoerde aan een nieuwe user
      $user->name = Input::get('name');
      $user->email = Input::get('email');
      $user->password = Input::get('password');

      $user->save();

      //Hier log ik de user in
      auth()->login($user);

      return redirect('/');










    }
}
