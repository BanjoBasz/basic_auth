<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; //<- deze functie doet de database logica

class SessionsController extends Controller
{
    public function create()
    {
       return view('sessions.create');
    }

    public function store()
    {
      if(Auth::attempt(request(['email','password'])))
      {
          //passed
          return redirect('/');
      }

      //fail
      return redirect('/login');

    }
}
