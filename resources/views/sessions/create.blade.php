<h1> Login </h1>

<form method ="POST" action="/login">
  <!-- Needed for Laravel -->
  {{ csrf_field() }}

  <!-- input email -->
  <label for = "email"> Email </label>
  <input type = "email" id="email" name="email">

  <!-- input password -->
  <label for = "password"> Password </label>
  <input type = "password" id="password" name="password">


  <button type="submit">Login  </button>

</form>
