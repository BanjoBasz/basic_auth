<h1> Register </h1>

<form method ="POST" action="/register">
  <!-- Needed for Laravel -->
  {{ csrf_field() }}

  <!-- input name -->
  <label for = "name"> Name </label>
  <input type = "text" id="name" name="name">

  <!-- input email -->
  <label for = "email"> Email </label>
  <input type = "email" id="email" name="email">

  <!-- input password -->
  <label for = "password"> Password </label>
  <input type = "password" id="password" name="password">


  <button type="submit">Register </button>

</form>
