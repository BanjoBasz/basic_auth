<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Deze route zorgt ervoor dat Users zich kunnen registreren
Route::get('/register','RegistrationController@create');

Route::post('/register','RegistrationController@store');


//Zorgt ervoor dat users gegevens kunnen invoeren
Route::get('/login','SessionsController@create');

//Zorgt ervoor dat users daadwerkelijk kunnen inloggen
Route::post('/login','SessionsController@store');
